# Fat Piggy

![](https://img.itch.zone/aW1nLzE1MDU2NDU4LnBuZw==/original/tGmDU%2B.png)

:es: Aventura en 3D de plataformas en la que juegas como un cerdo con la habilidad única de transformarse en una bola. Este juego es un prototipo desarrollado en Unity para la materia "Motores Gráficos" en UAI.
Programación, shaders y arte 3D hechos por mí.

:us: 3D platformer adventure where you play as a pig with the unique ability to transform into a ball. This game is a prototype developed in Unity for the "Graphics Engines" course at UAI.
Programming, shaders, and 3D art made by me.

[Play on itch.io](https://lucirro.itch.io/fat-piggy)