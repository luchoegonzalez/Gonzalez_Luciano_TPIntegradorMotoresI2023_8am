using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlParticulasEnemigo : MonoBehaviour
{
    void Start()
    {
        Invoke("Autodestruccion", 0.45f);
    }

    private void Autodestruccion()
    {
        Destroy(gameObject);
    }
}
