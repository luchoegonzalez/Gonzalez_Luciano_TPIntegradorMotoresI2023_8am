using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlDonaEnergia : MonoBehaviour
{
    private ControlJugador scriptJugador;
    // Start is called before the first frame update
    void Start()
    {
        scriptJugador = GameObject.Find("ControlJugador").GetComponent<ControlJugador>();
    }

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(new Vector3(0, 0, 180) * Time.deltaTime * 0.5f);
        Vector3 posicionMovimiento = transform.localPosition;
        posicionMovimiento.y = Mathf.Sin(Time.time * 0.8f) * 0.25f + 1.6f;
        transform.localPosition = posicionMovimiento;
    }

    private void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.CompareTag("Jugador") || collision.gameObject.CompareTag("JugadorBola"))
        {
            scriptJugador.energia += 25;
            transform.parent.gameObject.SetActive(false);
        }
    }

}
