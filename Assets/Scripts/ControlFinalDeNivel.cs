using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlFinalDeJuego : MonoBehaviour
{
    public GameManager scriptGameManager;
    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.CompareTag("Jugador") || collision.gameObject.CompareTag("JugadorBola"))
        {
            scriptGameManager.Ganaste();
        }
    }
}
