using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlBotonDona : MonoBehaviour
{
    private Animator animatorBoton;
    private GameObject dona;
    private ParticleSystem particulas;

    void Start()
    {
        dona = transform.parent.parent.GetChild(0).gameObject;
        animatorBoton = transform.GetChild(0).GetComponent<Animator>();
        particulas = transform.parent.parent.GetChild(3).GetComponent<ParticleSystem>();
    }

    private void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.CompareTag("Mano"))
        {
            animatorBoton.SetBool("apretado", true);
            dona.SetActive(true);
            particulas.Play();
        }
    }

    private void OnTriggerExit(Collider collision)
    {
        if (collision.gameObject.CompareTag("Mano"))
        {
            animatorBoton.SetBool("apretado", false);
        }
    }
}
