using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JefeControlCamara : MonoBehaviour
{
    public Transform posicionJefeFinal;
    public Transform transformJugador;

    private Vector2 mouseMirar;
    private Vector2 suavidadV;
    public float sensibilidad = 5.0f;
    public float suavizado = 2.0f;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        var md = new Vector2(0, Input.GetAxisRaw("Mouse Y"));
        md = Vector2.Scale(md, new Vector2(sensibilidad * suavizado, sensibilidad * suavizado));
        suavidadV.y = Mathf.Lerp(suavidadV.y, md.y, 1f / suavizado);
        mouseMirar += suavidadV; 
        mouseMirar.y = Mathf.Clamp(mouseMirar.y, -2f, 5f);
        transform.parent.localRotation = Quaternion.AngleAxis(-mouseMirar.y, Vector3.right);
    }

    private void LateUpdate()
    {
        posicionJefeFinal.LookAt(transformJugador);
        posicionJefeFinal.rotation = Quaternion.Euler(0, transform.rotation.eulerAngles.y + 180, 0f);
        posicionJefeFinal.position = new Vector3(posicionJefeFinal.position.x, transformJugador.position.y, posicionJefeFinal.position.z);
    }
}
