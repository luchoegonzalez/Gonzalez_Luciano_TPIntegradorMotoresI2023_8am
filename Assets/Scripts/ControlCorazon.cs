using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlCorazon : MonoBehaviour
{
    private ControlJugador scriptJugador;
    void Start()
    {
        scriptJugador = GameObject.Find("ControlJugador").GetComponent<ControlJugador>();
    }

    void Update()
    {
        transform.Rotate(new Vector3(0, 0, 180) * Time.deltaTime * 0.5f);
        Vector3 posicionMovimiento = transform.localPosition;
        posicionMovimiento.y = Mathf.Sin(Time.time * 0.8f) * 0.25f + 0.6f;
        transform.localPosition = posicionMovimiento;
    }

    private void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.CompareTag("Jugador") || collision.gameObject.CompareTag("JugadorBola"))
        {
            scriptJugador.vidaJugador += 25;
            transform.parent.gameObject.SetActive(false);
        }
    }

}