using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlPinchos : MonoBehaviour
{
    private GameObject jugador;
    private ControlJugador scriptJugador;

    private void Start()
    {
        jugador = GameObject.Find("ControlJugador");
        scriptJugador = jugador.GetComponent<ControlJugador>();
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("EnemigoGato"))
        {
            ComportamientoEnemigoGenerico scriptEnemigo = collision.gameObject.GetComponent<ComportamientoEnemigoGenerico>();
            scriptEnemigo.RecibirDano();
        }

        if (collision.gameObject.CompareTag("Jugador") || collision.gameObject.CompareTag("JugadorBola"))
        {
            scriptJugador.RecibirDano(100, gameObject);
        }
    }
}
