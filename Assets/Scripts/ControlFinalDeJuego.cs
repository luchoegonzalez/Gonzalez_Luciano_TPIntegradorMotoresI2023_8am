using UnityEngine;
using UnityEngine.SceneManagement;

public class ControlFinalDeNivel : MonoBehaviour
{
    public GameManager scriptGameManager;
    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.CompareTag("Jugador") || collision.gameObject.CompareTag("JugadorBola"))
        {
            SceneManager.LoadScene("EscenaJefeFinal");
        }
    }
}
