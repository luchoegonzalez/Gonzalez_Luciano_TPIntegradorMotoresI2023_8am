using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlModoBola : MonoBehaviour
{
    private Rigidbody rb;
    private SphereCollider colliderBola;
    private bool dashActivo = false;
    public Vector3 direccionActual;

    public int magnitudAceleracionDeDesplazamiento;
    public int rapidezMaxima;
    public int magnitudSalto;
    public int magnitudDash;
    private GameObject camara;
    public LayerMask capaPiso;
    public LayerMask capaGranja;

    public ControlJugador scriptControlJugador;


    void Start()
    {
        rb = GetComponent<Rigidbody>();
        colliderBola = GetComponent<SphereCollider>();
        direccionActual = transform.forward;
        camara = GameObject.Find("Main Camera");
    }

    private void FixedUpdate()
    {
        if (DireccionEnBaseACamara() != Vector3.zero && !dashActivo)
        {
            rb.AddForce(DireccionEnBaseACamara() * magnitudAceleracionDeDesplazamiento, ForceMode.Force);

            //funcion clamp para limitar la velocidad
            rb.velocity = Vector3.ClampMagnitude(rb.velocity, rapidezMaxima);

            //guardo la direccion para poder usarla en el dash a pesar de dejar de apretar las teclas "WASD"
            direccionActual = DireccionEnBaseACamara();
        }
    }


    void Update()
    {
        bool estaEnPiso = Physics.CheckSphere(new Vector3(colliderBola.bounds.center.x, colliderBola.bounds.min.y + 0.7f, colliderBola.bounds.center.z), colliderBola.radius * 0.18f, capaPiso) || Physics.CheckSphere(new Vector3(colliderBola.bounds.center.x, colliderBola.bounds.min.y + 0.7f, colliderBola.bounds.center.z), colliderBola.radius * 0.18f, capaGranja);
        
        if (Input.GetKeyDown(KeyCode.Space) && estaEnPiso)
        {
            rb.AddForce(Vector3.up * magnitudSalto, ForceMode.Impulse);
        }

        if (Input.GetKeyDown(KeyCode.E) && !dashActivo && estaEnPiso && scriptControlJugador.energia >= 15)
        {
            dashActivo = true;
            rapidezMaxima = 20000;
            rb.AddForce(new Vector3(camara.transform.forward.x, 0, camara.transform.forward.z).normalized * magnitudDash, ForceMode.Impulse);

            scriptControlJugador.energia -= 10;

            // freno el dash para que el aceleron sea muy intenso pero corto
            Invoke("FrenarDash", 1.5f);
        }
    }

    // Calculo la direccion a la que me voy a mover en base a la camara, con sus vectores forwward y right
    private Vector3 DireccionEnBaseACamara()
    {
        float movimientoHorizontal = Input.GetAxis("Horizontal");
        float movimientoVertical = Input.GetAxis("Vertical");

        Vector3 camaraVectorAdelante = new Vector3(camara.transform.forward.x, 0.0f, camara.transform.forward.z);
        Vector3 camaraVectorDerecha = new Vector3(camara.transform.right.x, 0.0f, camara.transform.right.z);

        camaraVectorAdelante.Normalize();
        camaraVectorDerecha.Normalize();

        Vector3 direccionDeMovimiento = camaraVectorAdelante * movimientoVertical + camaraVectorDerecha * movimientoHorizontal;
        direccionDeMovimiento.Normalize();

        return direccionDeMovimiento;
    }

    public void FrenarDash()
    {
        rapidezMaxima = 10;
        rb.velocity = Vector3.ClampMagnitude(rb.velocity, rapidezMaxima);
        dashActivo = false;
    }

}
