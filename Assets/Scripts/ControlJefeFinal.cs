using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlJefeFinal : MonoBehaviour
{
    public GameObject ojo1;
    public GameObject ojo2;
    public GameObject ojo3;

    List<GameObject> listaOjos;
    GameObject jugador;
    ControlJugador scriptJugador;
    public GameManager scriptGameManager;

    float vidaJefe = 100;

    void Start()
    {
        listaOjos = new List<GameObject> {ojo1, ojo2, ojo3};
        jugador = GameObject.Find("ControlJugador");
        scriptJugador = jugador.GetComponent<ControlJugador>();
    }

    // Update is called once per frame
    void Update()
    {
        foreach (GameObject ojo in listaOjos)
        {
            ojo.transform.LookAt(jugador.transform);
            ojo.transform.rotation = ojo.transform.rotation * Quaternion.Euler(new Vector3(80, 0, -180));
        }
    }

    public void RecibirDano(float dano)
    {
        vidaJefe -= dano;

        if (vidaJefe <= 0)
        {
            scriptGameManager.Ganaste();
        }
    }

    //private void OnCollisionEnter(Collision collision)
    //{
    //    if (collision.gameObject.CompareTag("Jugador") || collision.gameObject.CompareTag("JugadorBola"))
    //    {
    //        scriptJugador.RecibirDano(25, gameObject);
    //    }
    //}
}
