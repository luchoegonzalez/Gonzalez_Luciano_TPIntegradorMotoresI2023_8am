using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlDiana : MonoBehaviour
{
    public GameObject pisoBaseEnemigo;
    private Animator animatorDiana;
    private Animator animatorPiso;

    void Start()
    {
        animatorDiana = transform.parent.GetComponent<Animator>();
        animatorPiso = pisoBaseEnemigo.GetComponent<Animator>();
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.CompareTag("JugadorBola"))
        {
            if(collision.gameObject.GetComponent<Rigidbody>().velocity.magnitude > 4)
            {
                animatorDiana.SetBool("dianaActivada", true);
                animatorPiso.SetBool("dianaActivada", true);
            }
        }
    }
}
