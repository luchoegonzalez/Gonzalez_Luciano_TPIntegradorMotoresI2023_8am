using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ControlMenuPrincipal : MonoBehaviour
{
    public GameObject panelControles;
    public GameObject panelMenu;

    private void Start()
    {
        GestorDeAudio.instancia.ReproducirSonido("Musica");
    }

    public void Jugar ()
    {
        SceneManager.LoadScene("EscenaPrincipal");
    }

    public void Salir()
    {
        Debug.Log("Ya sali del juego");
        Application.Quit();
    }

    public void VerControles()
    {
        panelControles.SetActive(true);
        panelMenu.SetActive(false);
    }

    public void AtrasMenu()
    {
        panelMenu.SetActive(true);
        panelControles.SetActive(false);
    }
}
