using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlVidaYEnergia : MonoBehaviour
{
    public static ControlVidaYEnergia instancia;
    public float vida = 100;
    public float energia = 50;

    void Awake()
    {
        if (instancia == null)
        {
            instancia = this;
        }
        else
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);
    }

}
