using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlOjosJefe : MonoBehaviour
{
    ControlJefeFinal scriptJefeFinal;
    SphereCollider colliderEsfera;
    public GameObject parpadoArriba;
    public GameObject parpadoAbajo;
    private Animator animatorParpadoArriba;
    private Animator animatorParpadoAbajo;


    public ControlManoJefe scriptControlMano;

    void Start()
    {
        scriptJefeFinal = transform.parent.GetComponent<ControlJefeFinal>();
        colliderEsfera = GetComponent<SphereCollider>();
        animatorParpadoArriba = parpadoArriba.GetComponent<Animator>();
        animatorParpadoAbajo = parpadoAbajo.GetComponent<Animator>();
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("JugadorBola"))
        {
            if (collision.gameObject.GetComponent<Rigidbody>().velocity.magnitude > 4)
            {
                scriptJefeFinal.RecibirDano(34);
                colliderEsfera.enabled = false;
                animatorParpadoArriba.SetBool("golpeado", true);
                animatorParpadoAbajo.SetBool("golpeado", true);
                scriptControlMano.PausarMovimiento();
            }
        }
    }
}