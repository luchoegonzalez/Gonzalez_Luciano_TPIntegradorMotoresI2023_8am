using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class GameManager : MonoBehaviour
{
    public GameObject pantallaGameOver;
    public ControlCamara scriptControlCamara;
    public TMP_Text textoGameOver;
    public GameObject gestorDeAudioPrefab;

    ControlJugador scriptControlJugador;
    public GameObject barraDeVida;
    public GameObject barraDeEnergia;

    private string inputActual = "";

    private void Start()
    {
        if (GestorDeAudio.instancia == null)
        {
            Instantiate(gestorDeAudioPrefab);
            GestorDeAudio.instancia.ReproducirSonido("Musica");
        }
        scriptControlJugador = GameObject.Find("ControlJugador").GetComponent<ControlJugador>();
        Cursor.lockState = CursorLockMode.Locked;
    }

    public void ReiniciarNivel ()
    {
        scriptControlJugador.vidaJugador = 100;
        ControlVidaYEnergia.instancia.vida = 100;
        scriptControlJugador.energia = 50;
        ControlVidaYEnergia.instancia.energia = 50;
        SceneManager.LoadScene("EscenaPrincipal");
        Time.timeScale = 1;
    }

    public void Perdiste()
    {
        textoGameOver.text = "Perdiste :(";
        pantallaGameOver.SetActive(true);
        barraDeVida.SetActive(false);
        barraDeEnergia.SetActive(false);
        Time.timeScale = 0;
        if (scriptControlCamara != null)
        {
            scriptControlCamara.enabled = false;
        }
        Cursor.lockState = CursorLockMode.None;
    }

    public void Ganaste()
    {
        textoGameOver.text = "Ganaste!!!";
        pantallaGameOver.SetActive(true);
        barraDeVida.SetActive(false);
        barraDeEnergia.SetActive(false);
        Time.timeScale = 0;
        if (scriptControlCamara != null)
        {
            scriptControlCamara.enabled = false;
        }
        Cursor.lockState = CursorLockMode.None;
    }

    private void Update()
    {
        string inputCheat = "boss";

        if (Input.anyKeyDown)
        {
            inputActual += Input.inputString;

            if (inputActual.Contains(inputCheat))
            {
                scriptControlJugador.vidaJugador = 100;
                ControlVidaYEnergia.instancia.vida = 100;
                scriptControlJugador.energia = 50;
                ControlVidaYEnergia.instancia.energia = 50;
                SceneManager.LoadScene("EscenaJefeFinal");
                Time.timeScale = 1;
                inputActual = "";
            }
        }

        if (Input.GetKeyDown("escape"))
        {
            Cursor.lockState = CursorLockMode.None;
        }
    }
}
