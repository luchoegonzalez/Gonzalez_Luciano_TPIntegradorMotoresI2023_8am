using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlGatoDisparador : MonoBehaviour
{

    private GameObject brazoConArma; 
    private GameObject jugador;

    public LayerMask capaJugador;
    public float rangoDeDeteccion = 20;

    private float timerDisparo = 0;
    public float frecuenciaDeDisparo = 2;
    public GameObject balaPrefab;
    public float fuerzaProyectil = 1;

    // Start is called before the first frame update
    void Start()
    {
        brazoConArma = transform.GetChild(0).GetChild(0).GetChild(1).GetChild(0).GetChild(0).gameObject;
        jugador = GameObject.Find("ControlJugador");
    }

    // Update is called once per frame
    void Update()
    {
        bool jugadorCerca = Physics.CheckSphere(transform.position, rangoDeDeteccion, capaJugador);

        if (jugadorCerca)
        {
            brazoConArma.transform.LookAt(jugador.transform);
            brazoConArma.transform.rotation = brazoConArma.transform.rotation * Quaternion.Euler(new Vector3(-90, 0, 180));

            transform.LookAt(jugador.transform);
            transform.rotation = Quaternion.Euler(0f, transform.rotation.eulerAngles.y, 0f);

            timerDisparo += Time.deltaTime;
            if(timerDisparo >= frecuenciaDeDisparo)
            {
                timerDisparo = 0;
                DispararBala();
            }
        }
    }

    private void DispararBala()
    {
        GameObject bala;
        bala = Instantiate(balaPrefab, brazoConArma.transform.GetChild(0).transform.position, Quaternion.identity);

        Rigidbody rb = bala.GetComponent<Rigidbody>();
        rb.AddForce(brazoConArma.transform.up * fuerzaProyectil, ForceMode.Impulse);

        Destroy(bala, 5);
    }

}
