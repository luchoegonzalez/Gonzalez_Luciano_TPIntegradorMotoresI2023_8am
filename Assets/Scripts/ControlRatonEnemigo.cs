using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class ControlRatonEnemigo : MonoBehaviour
{
    private GameObject jugador;
    ControlJugador scriptJugador;

    private NavMeshAgent agenteNavegacion;
    public LayerMask capaJugador;
    public float rangoDeDeteccion = 6;

    private void Start()
    {
        jugador = GameObject.Find("ControlJugador");
        scriptJugador = jugador.GetComponent<ControlJugador>();
        agenteNavegacion = GetComponent<NavMeshAgent>();
    }

    private void Update()
    {
        bool estaCerca = Physics.CheckSphere(transform.position, rangoDeDeteccion, capaJugador);
        if (estaCerca)
        {
            if(scriptJugador.modoBola)
            {
                Vector3 direccion = transform.position - jugador.transform.position;
                Vector3 destino = transform.position + direccion.normalized * (rangoDeDeteccion / 2);
                agenteNavegacion.SetDestination(destino);
            } else
            {
                agenteNavegacion.SetDestination(jugador.transform.position);
            }
        }
    }
}
