using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlAguaColision : MonoBehaviour
{
    public ControlJugador scriptJugador;

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Jugador") || collision.gameObject.CompareTag("JugadorBola"))
        {
            scriptJugador.RecibirDano(100, collision.gameObject);

        }
    }
}
