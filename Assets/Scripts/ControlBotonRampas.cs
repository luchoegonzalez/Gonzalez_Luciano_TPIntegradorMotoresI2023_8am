using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlBotonRampas : MonoBehaviour
{
    private GameObject rampa;
    private Animator animatorBoton;
    private Animator animatorRampa;

    void Start()
    {
        rampa = transform.parent.parent.GetChild(1).GetChild(0).gameObject;
        animatorBoton = GetComponent<Animator>();
        animatorRampa = rampa.GetComponent<Animator>();
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("JugadorBola"))
        {
            animatorBoton.SetBool("apretado", true);
            animatorRampa.SetBool("rampaActivada", true);
        }
        
    }
}
