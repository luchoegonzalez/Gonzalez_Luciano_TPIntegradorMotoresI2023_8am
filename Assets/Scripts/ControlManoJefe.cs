using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlManoJefe : MonoBehaviour
{
    public float alturaMano = 6f;

    GameObject jugador;
    ControlJugador scriptJugador;
    GameObject camara;
    Vector3 posicionFlotando;

    Vector3 jugadorPosicionInicial;
    bool jugadorEnMovimiento = false;

    public bool siguiendoJugador = false;
    public bool golpeando = false;
    public bool golpeandoJugador = false;
    public bool recuperandose = false;
    public bool pausaMovimiento = false;

    // Start is called before the first frame update
    void Start()
    {
        jugador = GameObject.Find("ControlJugador");
        camara = GameObject.Find("Main Camera");
        scriptJugador = jugador.GetComponent<ControlJugador>();
        jugadorPosicionInicial = jugador.transform.position;
    }



    // Update is called once per frame
    void LateUpdate()
    {
        if (!jugadorEnMovimiento)
        {
            if (jugador.transform.position.x != jugadorPosicionInicial.x)
            {
                Recuperarse();
                jugadorEnMovimiento = true;
            }
        }

        if (!pausaMovimiento)
        {
            if (recuperandose)
            {
                posicionFlotando = jugador.transform.position;
                if (jugador.transform.position.y >= 6)
                {
                    posicionFlotando.y = jugador.transform.position.y + alturaMano;
                }
                else
                {
                    posicionFlotando.y = alturaMano;
                }
                transform.position = Vector3.Slerp(transform.position, posicionFlotando, Time.deltaTime / 0.3f);

                Vector3 direccion = camara.transform.position - transform.position;
                Quaternion rotacionDeseada = Quaternion.LookRotation(direccion);
                Quaternion rotacionFinal = Quaternion.Euler(-15.92f, rotacionDeseada.eulerAngles.y, 181.232f);

                transform.rotation = Quaternion.Slerp(transform.rotation, rotacionFinal, Time.deltaTime * 2f);
            }
            if (siguiendoJugador)
            {
                posicionFlotando = jugador.transform.position;

                if (jugador.transform.position.y >= 6)
                {
                    posicionFlotando.y = jugador.transform.position.y + alturaMano;
                } else
                {
                    posicionFlotando.y = alturaMano;
                }
                
                transform.position = Vector3.Slerp(transform.position, posicionFlotando, Time.deltaTime / 0.15f);

                Vector3 direccion = camara.transform.position - transform.position;
                Quaternion rotacionDeseada = Quaternion.LookRotation(direccion);
                Quaternion rotacionFinal = Quaternion.Euler(-15.92f, rotacionDeseada.eulerAngles.y, 181.232f);

                transform.rotation = Quaternion.Slerp(transform.rotation, rotacionFinal, Time.deltaTime * 2f);
            }
            if (golpeando)
            {
                posicionFlotando.y = 0.6f;
                transform.position = Vector3.Lerp(transform.position, posicionFlotando, Time.deltaTime / 0.5f);

            }
        }

    }

    void Golpear()
    {
        golpeando = true;
        siguiendoJugador = false;
        recuperandose = false;

        CancelInvoke("SeguirJugador");
        CancelInvoke("Recuperarse");
        CancelInvoke("Golpear");
    }

    void Recuperarse()
    {
        golpeando = false;
        siguiendoJugador = false;
        recuperandose = true;

        CancelInvoke("SeguirJugador");
        CancelInvoke("Recuperarse");
        CancelInvoke("Golpear");
        Invoke("SeguirJugador", 2f);
    }

    void SeguirJugador()
    {
        golpeando = false;
        siguiendoJugador = true;
        recuperandose = false;

        CancelInvoke("SeguirJugador");
        CancelInvoke("Recuperarse");
        CancelInvoke("Golpear");
        Invoke("Golpear", 4f);
    }

    public void PausarMovimiento()
    {
        golpeando = false;
        siguiendoJugador = false;
        recuperandose = false;
        pausaMovimiento = true;

        CancelInvoke("SeguirJugador");
        CancelInvoke("Recuperarse");
        CancelInvoke("Golpear");
        Invoke("ReanudarMovimiento", 3f);
    }

    void ReanudarMovimiento()
    {
        pausaMovimiento = false;
        Recuperarse();
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Jugador"))
        {
            scriptJugador.RecibirDano(25, camara.transform.parent.gameObject);
            if (golpeando)
            {
                CancelInvoke("SeguirJugador");
                CancelInvoke("Recuperarse");
                CancelInvoke("Golpear");
                Recuperarse();
            }
        }


        if (collision.gameObject.CompareTag("JugadorBola"))
        {
            scriptJugador.energia = 0.5f;
            if (golpeando)
            {
                CancelInvoke("SeguirJugador");
                CancelInvoke("Recuperarse");
                CancelInvoke("Golpear");
                Recuperarse();
            }
        }

        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Suelo"))
        {
            if (golpeando)
            {
                CancelInvoke("SeguirJugador");
                CancelInvoke("Recuperarse");
                CancelInvoke("Golpear");
                Invoke("Recuperarse", 4f);
            }
        }
    }
}
