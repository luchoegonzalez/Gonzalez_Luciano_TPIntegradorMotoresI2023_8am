using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ControlJugador : MonoBehaviour
{
    public GameObject personajeBola;
    public GameObject personajeNormal;
    public ParticleSystem particulasHumo;
    public ParticleSystem particulasEstrella;

    private ControlModoBola scriptControlBola;
    private ControlModoNormal scriptModoNormal;
    private GameObject jugador;
    public bool modoBola = false;

    public float vidaJugador = 100;
    public float energia = 0;
    public Image barraDeVida;
    public Image barraDeEnergia;

    public GameManager scriptGameManager;

    public bool camDirectaAJugador = true;


    void Start()
    {
        jugador = personajeNormal;
        scriptControlBola = personajeBola.GetComponent<ControlModoBola>();
        scriptModoNormal = personajeNormal.GetComponent<ControlModoNormal>();

        energia = ControlVidaYEnergia.instancia.energia;
        vidaJugador = ControlVidaYEnergia.instancia.vida;
    }

    void Update()
    {
        if (!camDirectaAJugador)
        {
            transform.position = jugador.transform.position;
        }

        if (modoBola)
        {
            if (energia > 0)
            {
                energia -= Time.deltaTime * 3;
            }
            else
            {
                TransformarEnNormal();
                energia = 0;
            }
        }

        if (Input.GetKeyDown(KeyCode.LeftControl))
        {
            if (modoBola)
            {
                TransformarEnNormal();
            } else if (!modoBola && energia >= 15)
            {
                TransformarEnBola();
            }
        }


        energia = Mathf.Clamp(energia, 0, 100);
        ControlVidaYEnergia.instancia.energia = energia;
        vidaJugador = Mathf.Clamp(vidaJugador, 0, 100);
        ControlVidaYEnergia.instancia.vida = vidaJugador;
        DibujarBarraDeEnergia();
        DibujarBarraDeVida();
    }

    public void TransformarEnBola()
    {
        particulasHumo.Play();
        GestorDeAudio.instancia.ReproducirSonido("Sonido Chancho");
        personajeBola.GetComponent<Rigidbody>().WakeUp();
        personajeBola.GetComponent<Rigidbody>().velocity = Vector3.zero;
        personajeBola.transform.localRotation = personajeNormal.transform.localRotation;
        personajeNormal.SetActive(false);
        personajeBola.transform.position = transform.position;
        personajeBola.SetActive(true);
        jugador = personajeBola;
        modoBola = true;
    }

    public void TransformarEnNormal ()
    {
        particulasHumo.Play();
        GestorDeAudio.instancia.ReproducirSonido("Sonido Chancho");
        scriptControlBola.FrenarDash();
        personajeBola.GetComponent<Rigidbody>().Sleep();
        personajeBola.SetActive(false);
        personajeNormal.transform.position = transform.position;
        personajeNormal.transform.localRotation = Quaternion.LookRotation(scriptControlBola.direccionActual);
        personajeNormal.SetActive(true);
        jugador = personajeNormal;
        modoBola = false;
    }

    private void LateUpdate()
    {
        if (camDirectaAJugador)
        {
            transform.position = jugador.transform.position;
        }
    }

    public void RecibirDano(float dano, GameObject enemigo)
    {
        vidaJugador -= dano;

        if(!enemigo.CompareTag("Bala"))
        {
            //stuneo al jugador con esta variable para que no se pueda mover durante un tiempo luego de recibir dano
            scriptModoNormal.recibiendoDano = true;
            //desplazo al jugador para que se aleje del enemigo al sufrir dano, haciendolo mas visual
            Vector3 vectorDireccionEnemigo = transform.position - enemigo.transform.position;
            vectorDireccionEnemigo.Normalize();
            jugador.GetComponent<Rigidbody>().AddForce((vectorDireccionEnemigo * 6f), ForceMode.Impulse);

            //habilito al jugador a moverse de vuelta
            Invoke("PermitirMovimiento", 0.5f);
        }

        if (vidaJugador <= 0)
        {
            jugador.SetActive(false);
            particulasEstrella.Play();
            Invoke("Perdiste", 1);
        }
    }

    private void PermitirMovimiento()
    {
        scriptModoNormal.recibiendoDano = false;
        jugador.GetComponent<Rigidbody>().velocity = Vector3.zero;
    }

    private void DibujarBarraDeEnergia()
    {
        barraDeEnergia.fillAmount = energia / 100;

        if(energia >= 15)
        {
            barraDeEnergia.color = new Color(0, 210, 255);
        } else
        {
            barraDeEnergia.color = Color.gray;
        }
    }

    private void DibujarBarraDeVida()
    {
        barraDeVida.fillAmount = vidaJugador / 100;
    }

    private void Perdiste()
    {
        scriptGameManager.Perdiste();
    }
}
