using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ComportamientoEnemigoGenerico : MonoBehaviour
{
    private GameObject jugador;
    private ControlJugador scriptJugador;
    public ParticleSystem particulasEnemigo;
    public bool gigante = false;

    private void Start()
    {
        jugador = GameObject.Find("ControlJugador");
        scriptJugador = jugador.GetComponent<ControlJugador>();
    }
    public void RecibirDano()
    {
        Instantiate(particulasEnemigo, transform.position, Quaternion.identity);

        GestorDeAudio.instancia.ReproducirSonido("Sonido Pop");
        Destroy(gameObject);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Jugador"))
        {
            scriptJugador.RecibirDano(25, gameObject);
        }

        if (collision.gameObject.CompareTag("JugadorBola"))
        {
            if (gigante)
            {
                scriptJugador.RecibirDano(100, gameObject);
            } else
            {
                scriptJugador.energia -= 25;
                RecibirDano();
            }
        }
    }
}
