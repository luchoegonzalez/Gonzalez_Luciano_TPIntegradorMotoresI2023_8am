using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlModoNormal : MonoBehaviour
{
    private Rigidbody rb;
    private CapsuleCollider colliderCapsula;
    private bool estaEnPiso = true;

    private GameObject camara;
    public float magnitudSalto = 5f;
    public LayerMask capaPiso;
    public LayerMask capaGranja;
    public float rapidezDesplazamiento = 3.5f;
    public float rapidezRotacion = 0.15f;

    private Animator animator;
    public string variableAnimacionMovimiento;
    public string variableAnimacionEstanEnSuelo;

    public bool recibiendoDano = false;
    public float multiplicadorDeGravedad;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        colliderCapsula = GetComponent<CapsuleCollider>();
        animator = GetComponent<Animator>();
        camara = GameObject.Find("Main Camera");
    }

    void Update()
    {
        estaEnPiso = Physics.CheckSphere(new Vector3(colliderCapsula.bounds.center.x, colliderCapsula.bounds.min.y, colliderCapsula.bounds.center.z), 0.15f, capaPiso) || Physics.CheckSphere(new Vector3(colliderCapsula.bounds.center.x, colliderCapsula.bounds.min.y, colliderCapsula.bounds.center.z), 0.15f, capaGranja);

        if (DireccionEnBaseACamara() != Vector3.zero && !recibiendoDano)
        {
            //ajusto la rotacion para que mire hacia donde se va a mover, usando la funcion Slerp que sirve para que se mueva suavemente desde su rotacion inicial hacia la rotacion deseada, haciendo una interpolacion para obtener los valores del medio
            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(DireccionEnBaseACamara()), rapidezRotacion);
            //me muevo con el vector que obtuve en el espacio del mundo
            transform.Translate(DireccionEnBaseACamara() * rapidezDesplazamiento * Time.deltaTime, Space.World);
        }

        if (Input.GetKeyDown(KeyCode.Space) && estaEnPiso && !recibiendoDano)
        {
            rb.AddForce(Vector3.up * magnitudSalto, ForceMode.Impulse);
        }

        if (!estaEnPiso)
        {
            //hago que caiga mas rapido al saltar
            rb.AddForce(Physics.gravity * multiplicadorDeGravedad * Time.deltaTime);
        }

        animator.SetBool(variableAnimacionEstanEnSuelo, estaEnPiso && !recibiendoDano);
    }

    private Vector3 DireccionEnBaseACamara()
    {
        float movimientoHorizontal = Input.GetAxis("Horizontal");
        float movimientoVertical = Input.GetAxis("Vertical");

        animator.SetFloat(variableAnimacionMovimiento, Mathf.Abs(movimientoVertical) + Mathf.Abs(movimientoHorizontal));

        Vector3 camaraVectorAdelante = new Vector3(camara.transform.forward.x, 0.0f, camara.transform.forward.z);
        Vector3 camaraVectorDerecha = new Vector3(camara.transform.right.x, 0.0f, camara.transform.right.z);

        camaraVectorAdelante.Normalize();
        camaraVectorDerecha.Normalize();

        Vector3 direccionDeMovimiento = camaraVectorAdelante * movimientoVertical + camaraVectorDerecha * movimientoHorizontal;
        direccionDeMovimiento.Normalize();

        return direccionDeMovimiento;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Enemigo") && !estaEnPiso)
        {
            ComportamientoEnemigoGenerico scriptEnemigo = other.transform.parent.gameObject.GetComponent<ComportamientoEnemigoGenerico>();
            scriptEnemigo.RecibirDano();

            //hago que pegue un salto luego de matar al enemigo, para hacerlo mas visual
            rb.AddForce(Vector3.up * 20, ForceMode.Impulse);
            
        }
    }

    private void FixedUpdate()
    {
        rb.velocity = Vector3.ClampMagnitude(rb.velocity, magnitudSalto);
    }

    // grafico la esfera que estoy usando para detectar colision con el piso
    private void OnDrawGizmos()
    {
        colliderCapsula = GetComponent<CapsuleCollider>();
        Gizmos.DrawSphere(new Vector3(colliderCapsula.bounds.center.x, colliderCapsula.bounds.min.y, colliderCapsula.bounds.center.z), 0.15f);
    }
}
