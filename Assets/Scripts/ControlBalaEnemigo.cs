using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlBalaEnemigo : MonoBehaviour
{
    private GameObject jugador;
    private ControlJugador scriptJugador;
    public ParticleSystem particulasBala;
    public float dano = 5;
    private void Start()
    {
        jugador = GameObject.Find("ControlJugador");
        scriptJugador = jugador.GetComponent<ControlJugador>();
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Jugador") || collision.gameObject.CompareTag("JugadorBola"))
        {
            scriptJugador.RecibirDano(dano, gameObject);
        }
        if (!collision.gameObject.CompareTag("EnemigoGato"))
        {
            Instantiate(particulasBala, transform.position, Quaternion.identity);
            Destroy(gameObject);
        }
    }
}
