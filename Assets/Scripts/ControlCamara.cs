using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlCamara : MonoBehaviour
{
    public Transform transformJugador;

    private Vector2 mouseMirar;
    private Vector2 suavidadV;
    private GameObject containerCamara;
    public float sensibilidad = 5.0f;
    public float suavizado = 2.0f;

    void Start()
    {
        containerCamara = this.transform.parent.gameObject;
    }

    private void Update()
    {
        var md = new Vector2(Input.GetAxisRaw("Mouse X"), Input.GetAxisRaw("Mouse Y"));
        md = Vector2.Scale(md, new Vector2(sensibilidad * suavizado, sensibilidad * suavizado));
        suavidadV.x = Mathf.Lerp(suavidadV.x, md.x, 1f / suavizado);
        suavidadV.y = Mathf.Lerp(suavidadV.y, md.y, 1f / suavizado);
        mouseMirar += suavidadV; mouseMirar.y = Mathf.Clamp(mouseMirar.y, -80f, 10f);
        transform.localRotation = Quaternion.AngleAxis(-mouseMirar.y, Vector3.right);
        containerCamara.transform.localRotation = Quaternion.AngleAxis(mouseMirar.x, containerCamara.transform.up);
    }

    private void LateUpdate()
    {
        containerCamara.transform.position = new Vector3(transformJugador.position.x, transformJugador.position.y, transformJugador.position.z);
    }
}
